#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	// Setup grabber
	grabber.setup(1280, 720);
	ofSetDataPathRoot("data/model/");
	
	// Setup fluid
	fluid.allocate(1280, 720, 0.5);
	fluid.dissipation = 0.90;
	fluid.velocityDissipation = 0.81;
	fluid.setGravity(ofVec2f(0.0, 0.9));//set gravity to make the fluid pour down

	// Setup tracker
	tracker.setup();


	ofSetBackgroundColor(2, 64, 232, 255);
	//ofSetBackgroundColor(0, 0, 0, 255);
	
}

//--------------------------------------------------------------
void ofApp::update() {
	grabber.update();
	// Update tracker when there are new frames
	if (grabber.isFrameNew()) {
		tracker.update(grabber);
	}
	//sets up the smoke eyes
	for (auto face : tracker.getInstances()) { //go thru all faces found

		ofxFaceTracker2Landmarks markers = face.getLandmarks(); //get the landmarks for this face

		glm::vec2 lEyeT = markers.getImagePoint(38); //get the points for the top center of each eye
		glm::vec2 rEyeT = markers.getImagePoint(43);
		glm::vec2 lEyeB = markers.getImagePoint(40); //get the points for the top center of each eye
		glm::vec2 rEyeB = markers.getImagePoint(47);

		float lDist = ofDist(lEyeT.x, lEyeT.y, lEyeB.x, lEyeB.y);//calculate distance from top and bottom of eye
		float rDist = ofDist(rEyeT.x, rEyeT.y, rEyeB.x, rEyeB.y);

		lEye += (lEyeB - lEye) * .3; //easing the positions
		rEye += (rEyeB - rEye) * .3;

		float time = ofGetElapsedTimef();

		//make the color for smoke
		ofFloatColor sColor(0.768, 0.803, 0.811);
		
		//add the emmiters for each eye, using the distance to adjust size and flow. 
		fluid.addTemporalForce(lEye, ofPoint(sin(time / 2.7), -lDist), sColor, lDist / 2.0);
		fluid.addTemporalForce(rEye, ofPoint(cos(time / 3.2), -rDist), sColor, rDist / 2.0);

	}
	fluid.update();
}

//--------------------------------------------------------------
void ofApp::draw() {

	//get facetracking data and use it to draw facial features
	ofPushStyle();
	ofPolyline lBrow;
	ofPolyline rBrow;
	ofPolyline noseBridge;
	ofPolyline mouth;
	// Iterate over all faces
	for (auto face : tracker.getInstances()) {
		// Apply the pose matrix
		ofPushView();
		face.loadPoseMatrix();
		ofxFaceTracker2Landmarks parts = face.getLandmarks();
		//get points that correspond to each face feature
		lBrow = parts.getImageFeature(parts.LEFT_EYEBROW);
		rBrow = parts.getImageFeature(parts.RIGHT_EYEBROW);
		noseBridge = parts.getImageFeature(parts.NOSE_BRIDGE);
		mouth = parts.getImageFeature(parts.OUTER_MOUTH);
		ofPopView();
	}
	ofPopStyle();

	//draw freatures
	ofEnableAlphaBlending();
	fluid.draw();
	lBrow.draw();
	rBrow.draw();
	noseBridge.draw();
	mouth.draw();
	ofDisableAlphaBlending();
}

