# Program 1 disapearing face #
![](screenshot.png)
this program made in openFrameworks which makes a ghostly face appear on a blue background using face tracking. 
It was inspired by early internt art that has been lost due to the websites and programs they were made in being abandond and no longer maintained

### Dependencies ###

 You will need the following ofxaddons:
* [ofxfacetracker2](https://github.com/HalfdanJ/ofxFaceTracker2)
* [ofxfluid](https://github.com/patriciogonzalezvivo/ofxFluid)
Note these addons have additional dependencies of ther own

